const User = require('../models/User')
const userController = {
  userList: [
    { id: 1, name: 'jennie', gender: 'F' },
    { id: 2, name: 'rose', gender: 'F' }
  ],
  lastId: 3,
  async addUser(req, res, next) {
    const payload = req.body
    console.log(payload)
    const user = new User(payload)
    try {
      const newuser = await user.save()
      res.json(newuser)
    } catch (err) {
      res.status(500).send(err)
    }

  },
  async updateUser(req, res, next) {
    const payload = req.body
    try {
      console.log(payload)
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers(req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }

  },
  async getUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
